public class Main {
    public static void main(String[] args) {
        Chat chat = new Chat();
        Runnable r1 = new UsuarioPregunta(chat);
        Runnable r2 = new UsuarioRespuesta(chat);
        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);

        t1.start();
        t2.start();
    }
}
