public class Chat {
    private boolean empty = true;

    public synchronized void pregunta(String msg) {
        while (!empty) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
        empty = false;
        System.out.println(msg);
        notify();
    }

    public synchronized void respuesta(String msg) {
        while (empty) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
        empty = true;
        System.out.println(msg);
        notify();
    }
}
