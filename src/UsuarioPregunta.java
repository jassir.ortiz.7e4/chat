public class UsuarioPregunta implements Runnable{
    private Chat chat;
    public UsuarioPregunta(Chat c) {
        this.chat = c;
    }
    @Override
    public void run() {
        chat.pregunta("Hola, como estas?");
    }
}
